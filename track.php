<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

include"config.php";
$track_id = $_POST['id'];
include('includes/header.php');

$album = mysql_query("SELECT album_name, album.album_id FROM album INNER JOIN track_album ON album.album_id = track_album.album_id WHERE track_album.track_id = $track_id");
$album_count = mysql_num_rows($album);

if($album_count != 0) {
	$row = mysql_fetch_array($album);
	$album_id = $row['album_id'];
	$album_name = '<a href="album.php?id=' . $row['album_id'] .'">' . $row['album_name'] .'</a>';
}

$artist = mysql_query("SELECT artist_name, artist.artist_id FROM artist INNER JOIN album_artist ON artist.artist_id = album_artist.artist_id WHERE album_artist.album_id = $album_id");
$artist_count = mysql_num_rows($artist);

if($artist_count != 0) {
	$row = mysql_fetch_array($artist);
	$artist_name = '<a href="artist.php?id=' . $row['artist_id'] .'">' . $row['artist_name'] .'</a>';
}

if(isset($_SESSION['username'])) {
	$username = $_SESSION['username'];
	$query = mysql_query("SELECT * FROM user WHERE username = '$username'");
	$count = mysql_num_rows($query);

	if($count != 0)
	{
		$row = mysql_fetch_array($query);
		$user_id = $row['user_id'];
	}
}
$query = mysql_query("SELECT * FROM track WHERE track_id = '$track_id'");
$count = mysql_num_rows($query);
?>
	<section>
		<header>
			<h1>Tracks</h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
<?php
if($count != 0)
{
	$row = mysql_fetch_array($query);
	$track_name = $row['track_name'];
	$track_url = $row['track_url'];
	echo '			<h2>' . $track_name . '</h2>';
}
else {
	echo 'Track does not exist.';
}
if(isset($_POST['add'])) {
	$track = $_POST['track_id'];
	$user_id = $_POST['user_id'];
	$favourite_date = date('Y-m-d H:i:s');

	$follow_track = "INSERT INTO favourite_track (track_id, user_id, favourite_date) VALUES ($track_id, $user_id, '$favourite_date')";
	
	$result = mysql_query($follow_track);
	
	if($result) {
		echo '<div class="alert alert-success">Followed ' . $track_name .'!</div>';
	}
	else {
		echo "Error: " . mysql_error();
	}
}
if(isset($_POST['delete'])) {
	$track_id = $_POST['track_id'];
	$user_id = $_POST['user_id'];

	$unfollow_track = "DELETE FROM favourite_track WHERE track_id = $track_id AND user_id = $user_id";
	
	$result = mysql_query($unfollow_track);
	
	if($result) {
		echo '<div class="alert alert-error">Unfollowed ' . $track_name . '!</div>';
	}
	else {
		echo "Error: " . mysql_error();
	}
}

$favourited_by = mysql_query("SELECT track_name, username FROM track INNER JOIN favourite_track ON track.track_id = favourite_track.track_id INNER JOIN user ON user.user_id = favourite_track.user_id WHERE track.track_id = $track_id");
$favourite_count = mysql_num_rows($favourited_by);
?>
<p>Artist: <?php echo $artist_name ?></p>
<p>Album: <?php echo $album_name ?></p>
<hr>
<form method="post">
	<input type="hidden" name="track_id" value="<?php echo $track_id ?>">
	<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
	<?php
	if(isset($_SESSION['username'])) {
		$query = mysql_query("SELECT * FROM favourite_track WHERE track_id = '$track_id' AND user_id = '$user_id'");
		$count = mysql_num_rows($query);

		if($count != 0)
		{
			echo '<input type="submit" class="btn btn-danger" value="Unfavourite ' . $track_name . '" name="delete">';
		}
		else {
			echo '<input type="submit" class="btn btn-success" value="Favourite ' . $track_name . '" name="add">';
		}
	}
	else {
		echo '<a href="login.php" class="btn btn-success disabled">You must be signed in to favourite a track.</a>';
	}
	
	?>
</form>
<h4>Favourited By</h4>
<?php
if ($favourite_count != 0) {
	while($row = mysql_fetch_array($favourited_by)) {
		$gravatar = new Gravatar();
		$username = $row['username'];
		$email = $gravatar->get_email_for_user($username);
		echo '<a href="member.php?username=' . $username . '"><img src="' . $gravatar->url($email) . '"/></a>';
	}
}
else {
	echo "No favourites.";
}

?>

<?php
include('includes/footer.php'); 
?>