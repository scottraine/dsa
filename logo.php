<?php
?>
<object>
	<svg version="1.1" id="Sheep" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
		<circle stroke="#2C75FF" stroke-width="3" stroke-miterlimit="10" cx="49.326" cy="48.826" r="46"/>
		<circle stroke="#2C75FF" stroke-width="3" stroke-miterlimit="10" cx="49.326" cy="48.826" r="41"/>
		<circle stroke="#2C75FF" stroke-width="3" stroke-miterlimit="10" cx="49.326" cy="48.826" r="36"/>
		<path stroke="#2C75FF" stroke-width="3" stroke-miterlimit="10" d="M23.778,36.637c0.228,1.488,10.159-12.451,25.548-12.041
			c7.208,0.191,15.562,3.204,24.559,11.639c2.572,2.413-4.5-12.857,17.359,9.162c4.122,4.152-12.217,0.362-12.217,0.362
			c6.453,0.256-32.102,90.937-59.312,0.441c-1.713-5.699-13.02,1.93-12.216-0.963C8.305,42.344,22.611,29.003,23.778,36.637z"/>
		<path stroke="#2C75FF" stroke-width="3" stroke-miterlimit="10" d="M21.806,95.245c0,0,13.931-30.062,25.075,0
			C48.251,98.941,18.591,97.014,21.806,95.245z"/>
		<path stroke="#2C75FF" stroke-width="3" stroke-miterlimit="10" d="M52.301,94.552c-1.484,3.651,28.221,2.648,25.062,0.78
			C77.363,95.332,64.376,64.852,52.301,94.552"/>
	</svg>
</object>