<?php
$track_id = $_GET['id'];

$sql = "
SELECT
track_name,
track_url,
artwork,
artist_name
FROM track
INNER JOIN track_album
ON track.track_id = track_album.track_id
INNER JOIN album
ON track_album.album_id = album.album_id
INNER JOIN album_artist
ON album.album_id = album_artist.album_id
INNER JOIN artist
ON album_artist.artist_id = artist.artist_id
WHERE track.track_id = $track_id
";
$player_info = mysql_query($sql);
$counter = mysql_num_rows($player_info);
$value = mysql_fetch_array($player_info);
if ($counter != 0){
	$hide = "";
	$play = "autoplay";
}
else {
	$hide = "hide";	
	$play = "";
}
?>
	<footer class="<?php echo $hide?>">
		<div class="music">
			<!-- Image of Artist/Track/Album/User -->
<?php echo '			<img src="artwork/'.$value['artwork'].'" />' ;?>
		</div>
		<div class="player">
			<!-- From Keith, HTML5 for Web Designers, p.35 pub.A Book Apart -->
			<audio controls <?php echo $play?>> 
<?php echo '				<source src="music/'.$value['track_url'].'" type="audio/mpeg">';?>
<?php echo '								<object type="application/x-shockwave-flash" » data="player.swf?soundFile=music/'.$value['track_url'].'"> ';?>
<?php echo '									<param name="movie" » value="player.swf?soundFile=music/'.$value['track_url'].'"> </param>';?>
<?php echo '									<a href="music/'.$value['track_url'].'">Download the song</a> ';?>
				</object> 
			</audio>
		</div>
		<div class="track">
<?php echo '							<p>Now playing...  '.$value['track_name'].' <span>'.$value['artist_name'].'</span></p>';?>
		</div>
	</footer>