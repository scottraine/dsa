<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}


$page_title = "Register";
include('config.php'); 
if(isset($_POST['submit'])) {
	$username = $_POST['username'];
	$email = $_POST['email'];
	if(userExists($username, $email)) {
		echo 'User is already registered.';
	}
	else {
		$salt = time();
		$password = $_POST['password'];
		$hashed_password = sha1($password . $salt);

		$add_user = 'INSERT into user (username, email, hashed_password, salt) VALUES ("'.$username.'" , "'.$email.'" , "'.$hashed_password.'", "'.$salt.'")';

		$result = mysql_query($add_user);

		if($result) {
			header("Location: success.php");
		}
		else {
			echo "Error: " . mysql_error();
		}
	}
}
include('includes/header.php'); 
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<div class="third">
				<form method="post">
					<label class="whole" for="id_username">Username:</label>
					<input class="whole" type="text" name="username" />
					<label class="whole" for="id_email">E-mail:</label>
					<input class="whole" type="email" name="email" />
					<label class="whole" for="id_password">Password:</label>
					<input class="whole" type="password" name="password" />
					<input class="button blue" type="submit" name="submit" id="id_submit" value="Register">
				</form>
			</div>
			<div class="third">
				<h2>Already registered?</h2>
				<a class="button orange" href="login.php">Sign in</a>
			</div>
		</article>
		<aside>
		</aside>
	</section>
<?php include('includes/footer.php'); ?>