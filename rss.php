<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

header("Content-Type: application/rss+xml");
require_once('config.php');

$query = "
(
SELECT artist_name AS name, username, favourite_artist.user_id, favourite_artist.artist_id, favourite_artist.favourite_date
FROM favourite_artist
INNER JOIN user ON favourite_artist.user_id = user.user_id
INNER JOIN artist ON favourite_artist.artist_id = artist.artist_id
)
UNION ALL
(
SELECT track_name, username, favourite_track.user_id, favourite_track.track_id, favourite_track.favourite_date
FROM favourite_track
INNER JOIN user ON favourite_track.user_id = user.user_id
INNER JOIN track ON favourite_track.track_id = track.track_id
)
ORDER BY favourite_date DESC 
LIMIT 10
";

$result = mysql_query($query);

echo '<?xml version="1.0" encoding="ISO-8859-1"?>';
echo '<rss version="2.0">';
echo '<channel>';
echo '<title>Awesome RSS favourites feed</title>';
echo '<description>Keep up to date with who is favouriting who</description>';
echo '<link>http://www.cems.uwe.ac.uk/~s2-raine/dsa/</link>';
while ($row = mysql_fetch_array($result)) {
	echo '<item>';
	echo '<title>'.$row['name'].'</title>';
	echo '<link>http://www.cems.uwe.ac.uk/~s2-raine/dsa/'.isArtist($row['artist_id']).'.php?id='.$row['artist_id'].'</link>';
	echo '<description>Favourited by '.$row['username'].'</description>';
	echo '</item>';
}
echo '</channel>';
echo '</rss>';
?>