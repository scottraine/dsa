<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Update Profile";
include('includes/header.php');
if(!isset($_SESSION['username']) && !isset($_SESSION['password'])) {
	header("Location: index.php");
}
$username = $_SESSION['username'];
$query = mysql_query("SELECT * FROM user WHERE username='$username'");
$row = mysql_fetch_array($query);
if(isset($_POST['submit'])) {
	$email = $_POST['email'];
	$bio = $_POST['bio'];

	$edit_user = "UPDATE user SET email='$email', bio='$bio' WHERE username='$username'";

	$result = mysql_query($edit_user);
	
	if($result) {
		header("Location: member.php?username=$username");
	}
	else {
		echo "Error: " . mysql_error();
	}
}
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<form class="third" method="post">
				<label  class="whole" for="id_email">Email:</label>
				<input  class="whole" type="email" name="email" id="id_email" value="<?php echo $row['email']; ?>"  />
				<label  class="whole" for="id_bio">Bio:</label>
				<textarea  class="whole" id="id_bio" name="bio" cols="30" rows="10"><?php echo $row['bio']; ?></textarea>
				<input type="submit" name="submit" id="id_submit" class="whole button orange" value="Update profile">
				<a href="member.php?username=<?php echo $username ?>" class="button blue">My Profile</a>
			</form>
		</article>
		<aside>
		</aside>
	</section>
<?php include('includes/footer.php'); ?>