<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Tracks";
include('includes/header.php');
// set variables
$page = $_SERVER['PHP_SELF'];
// grab user id of signed in user
if(!isset($userid)) {
	if(isset($_SESSION['username'])) {
		$username = $_SESSION['username'];
		$query = mysql_query("SELECT * FROM user WHERE username = '$username'");
		$count = mysql_num_rows($query);

		if($count != 0)
		{
			$row = mysql_fetch_array($query);
			$user_id = $row['user_id'];
		}
	}
}
// If add form is submitted, add user and track to favourite_track table
if(isset($_POST['add'])) {
	$track_id = $_POST['track_id'];
	$favourite_date = date('Y-m-d H:i:s');

	$follow_track = "
		INSERT INTO 
		favourite_track (
		track_id, 
		user_id, 
		favourite_date
		) 
		VALUES (
		$track_id, 
		$user_id, 
		'$favourite_date'
		)
	";
	$result = mysql_query($follow_track);
}

// If delete form is submitted, delete user and track to favourite_track table
if(isset($_POST['delete'])) {
	$track_id = $_POST['track_id'];
	$unfollow_track = "
		DELETE FROM 
		favourite_track 
		WHERE track_id = $track_id 
		AND user_id = $user_id
	";

	$result = mysql_query($unfollow_track);
}

?>
	<section>
		<header>
			<div class="half">
				<h1><?php echo $page_title ?></h1>
			</div>
			<div class="half">			
			<!-- Sorted by... Drop Down list -->
<?php echo '				<form action="'.$page.'" method="post" name="test" >'; ?>
					<select  class="button blue"  name="order" onchange="this.form.submit();" id="sort">
						<option <?php if ($_POST['order'] == 'added') print 'selected="selected" '; ?> value="added">most recently added</option>
						<option <?php if ($_POST['order'] == 'favourited') print 'selected="selected" '; ?> value="favourited">most favourited</option>
		    		</select>
		    	</form>
		    </div>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<table><!-- Start of Table-->
<?php
//check if favourited or recently added
if($_POST['order']=='favourited'){
	$order_by = 'favourite_track_count';
}
else {
	$order_by = 'track_added_date';
}

$track_list = mysql_query("
	SELECT 
	track.track_id AS track_id, 
	track_name,
	album.album_id AS album_id,
	album_name,
	artist.artist_id AS artist_id,
	artist_name,
	COUNT(favourite_track.track_id) AS favourite_track_count,
	track_added_date
	FROM track
	LEFT JOIN favourite_track ON track.track_id = favourite_track.track_id 
	INNER JOIN track_album ON track_album.track_id = track.track_id
	INNER JOIN album ON album.album_id = track_album.album_id
	INNER JOIN album_artist ON album.album_id = album_artist.album_id
	INNER JOIN artist ON artist.artist_id = album_artist.artist_id
	GROUP BY track_name
	ORDER BY $order_by DESC
");
//display results as table
echo '					<thead>';
echo '						<th class="fifth">Track Name</th>';
echo '						<th class="fifth">Album Name</th>';
echo '						<th class="fifth">Artist Name</th>';
echo '						<th class="fifth">Favourite</th>';
echo '						<th class="fifth">Favourited by</th>';
echo '					</thead>';
while($data = mysql_fetch_array($track_list)) {
	$track_id = $data['track_id'];
	echo '					<tr>';
	echo '						<td class="fifth"><a href="index.php?id=' . $data['track_id'] . '">' . $data['track_name'] . '</a></td>';
	echo '						<td class="fifth"><a href="album.php?id=' . $data['album_id'] . '">' . $data['album_name'] . '</a></td>';
	echo '						<td class="fifth"><a href="artist.php?id=' . $data['artist_id'] . '">' . $data['artist_name'] . '</a></td>';
	echo '						<td class="fifth">';
//favourite button
	echo '							<form method="post">';
	if(isset($_SESSION['username'])) {
		$query = mysql_query("
			SELECT 
			* 
			FROM favourite_track 
			WHERE track_id = $track_id
			AND user_id = $user_id
		");
		$count = mysql_num_rows($query);
		echo '								<input type="hidden" name="track_id" value="'.$track_id.'">';
		echo '								<input type="hidden" name="user_id" value="'.$user_id.'">';
		if($count != 0){
			echo '<input type="submit" class="button pink" value="Unfavourite" name="delete">';
		}
		else {
			echo '<input type="submit" class="button blue" value="Favourite" name="add">';
		}
	}
	else {
		echo '<a href="login.php" class="button orange">Sign in</a>';
	}
	echo '						</form></td>';
//return avatars of top 5 users who have favourited	track
	echo '						<td class="fifth">';
	if (isset($track_id)) {
		$favourited_by = mysql_query("
			SELECT
			track_name, 
			username 
			FROM track 
			INNER JOIN favourite_track 
			ON track.track_id = favourite_track.track_id 
			INNER JOIN user ON user.user_id = favourite_track.user_id
			WHERE track.track_id = $track_id
			LIMIT 5
		");			
			$favourite_count = mysql_num_rows($favourited_by);
			if ($favourite_count != 0) {
				while($row = mysql_fetch_array($favourited_by)) {
					$gravatar = new Gravatar();
					$username = $row['username'];
					$email = $gravatar->get_email_for_user($username);
					echo '<a href="member.php?username=' . $username . '"><img src="' . $gravatar->url($email) . '"/></a>';
				}
			}
			echo '</td></tr>';
		}
	}

?>
			</table><!-- End of Table-->
		</article>
		<aside class="outer">
<?php include('favourites.php');?>
		</aside>
	</section>
<?php include('player.php'); ?>
<?php include('includes/footer.php'); ?>