<?php include('includes/header.php'); ?>
	<section>
		<header>
			<h1>Registration</h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<h2>Registration Successful!</h2>
			<p>You have successfully registered.</p>
			<a href="login.php" class="button blue">Click here to login</a>
		</article>
		<aside>
		</aside>
	</section>
<?php include('includes/footer.php'); ?>