<?php
require_once('config.php');
ob_start();
?>
<!DOCTYPE HTML>
<head>
	<title><?php echo "Electric Sheep - ".$page_title ?></title>
	<link href='http://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css" type="text/css" />

	<script type="text/javascript" href="js/modernizr.custom.27132.js"></script>
	<script type="text/javascript" href="js/jquery-1.9.1.min.js"></script>
	<script src="js/twitter.js"></script>	
	<script type="text/javascript">
				$(function() {
    $('#mainContent div:not(:first)').hide();

    $('ul li a').click(function() {
        $('ul#nav li a').removeClass('selected');
        $(this).addClass('selected');

        var href = $(this).attr('href');
        var split = href.split('#');

        $('#mainContent div').hide();
        $('#mainContent div#' + split[1]).fadeIn();

        return false;
    });
});// this script is used to load and build the news content section which displays the rss items from the external news sites
</script>
</head>
<body>
	<div id="container">
		<header id="header">
			<!-- Logo: This is the logo to the site and an anchor to the homepage-->		
			<a href="index.php">
				<div class="logo outer">
				</div>
			</a>
			<div class="masthead inner">
				<!-- Site Name -->
				<h1>
					<a class="brand" href="index.php">Electric Sheep</a>
				</h1>
			</div>					
			<ul class="outer nav">
<?php
if(isset($_SESSION['username']) && isset($_SESSION['password'])) {
	echo "					<li><a href=\"member.php?username=" . $_SESSION['username'] . "\">My Profile</a></li>";
	echo "					<li><a href=\"logout.php\">Logout</a></li>";
}
else {
	echo "<li><a href='login.php'>Login</a></li>";
	echo "<li><a href='register.php'>Register</a></li>";
}
?>
			</ul><div class="masthead inner">
			<!-- Tag Line -->
			<blockquote>a social hub of information about all things nu-metal</blockquote>
		</div>					
	</header>