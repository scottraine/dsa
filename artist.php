<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

include"config.php";
$artist_id = $_REQUEST['id'];
$page_title = "Artists";
include('includes/header.php');

if(isset($_SESSION['username'])) {
	$username = $_SESSION['username'];
	$query = mysql_query("SELECT * FROM user WHERE username = '$username'");
	$count = mysql_num_rows($query);

	if($count != 0)
	{
		$row = mysql_fetch_array($query);
		$user_id = $row['user_id'];
	}
}
$query = mysql_query("SELECT * FROM artist WHERE artist_id = '$artist_id'");
$count = mysql_num_rows($query);

if($count != 0)
{
	$row = mysql_fetch_array($query);
	$artist_name = $row['artist_name'];
}
else {
	echo 'Artist does not exist.';
}
if(isset($_POST['add'])) {
	$artist_id = $_POST['artist_id'];
	$user_id = $_POST['user_id'];
	$favourite_date = date('Y-m-d H:i:s');

	$follow_artist = "INSERT INTO favourite_artist (artist_id, user_id, favourite_date) VALUES ($artist_id, $user_id, '$favourite_date')";
	
	$result = mysql_query($follow_artist);
	
	if($result) {
		echo '<div class="alert alert-success">Followed ' . $artist_name .'!</div>';
	}
	else {
		echo "Error: " . mysql_error();
	}
}
if(isset($_POST['delete'])) {
	$artist_id = $_POST['artist_id'];
	$user_id = $_POST['user_id'];

	$unfollow_artist = "DELETE FROM favourite_artist WHERE artist_id = $artist_id AND user_id = $user_id";
	
	$result = mysql_query($unfollow_artist);
	
	if($result) {
		echo '<div class="alert alert-error">Unfollowed ' . $artist_name . '!</div>';
	}
	else {
		echo "Error: " . mysql_error();
	}
}

$favourited_by = mysql_query("SELECT artist_name, username FROM artist INNER JOIN favourite_artist ON artist.artist_id = favourite_artist.artist_id INNER JOIN user ON user.user_id = favourite_artist.user_id WHERE artist.artist_id = $artist_id");
$favourite_count = mysql_num_rows($favourited_by);

$albums = mysql_query("SELECT * FROM album INNER JOIN album_artist ON album.album_id = album_artist.album_id WHERE artist_id = '$artist_id'");
$album_count = mysql_num_rows($albums);


$tracks = mysql_query("
SELECT * 
FROM track 
INNER JOIN track_album 
ON track_album.track_id = track.track_id 
INNER JOIN album 
ON track_album.album_id = album.album_id
INNER JOIN album_artist
ON album.album_id = album_artist.album_id
WHERE artist_id = '$artist_id'
ORDER BY track_name
");
$track_count = mysql_num_rows($tracks);

?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner-small">
			<div>
<?php echo'			<h2>'.$artist_name.'</h2>';?>
				<form method="post">
					<input type="hidden" name="artist_id" value="<?php echo $artist_id ?>">
					<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
<?php
if(isset($_SESSION['username'])) {
	$query = mysql_query("SELECT * FROM favourite_artist WHERE artist_id = '$artist_id' AND user_id = '$user_id'");
	$count = mysql_num_rows($query);

	if($count != 0)
	{
		echo '<input type="submit"  class="button pink" value="Unfavourite ' . $artist_name . '" name="delete">';
	}
	else {
		echo '<input type="submit"  class="button blue" value="Favourite ' . $artist_name . '" name="add">';
	}
}
else {
	echo '<a href="login.php"  class="button orange">You must be signed in to favourite an artist.</a>';
}

?>
				</form>
				<h2>Favourited by:</h2>
<?php
if ($favourite_count != 0) {
	while($row = mysql_fetch_array($favourited_by)) {
		$gravatar = new Gravatar();
		$username = $row['username'];
		$email = $gravatar->get_email_for_user($username);
		echo '					<a href="member.php?username=' . $username . '"><img src="' . $gravatar->url($email) . '"/></a>';
	}
}
else {
	echo "No favourites.";
}
?>

			</div>
			<div>
					<h2>Albums</h2>
<?php
echo '					<div class="whole">';
if($album_count != 0)
{
	while($row = mysql_fetch_array($albums)) {
		echo '					<div class="half">';
		echo '						<a href="album.php?id=' . $row['album_id'] . '">';
		echo '							<p>' . $row['album_name'] . '</p>';
		echo '							<img class="artwork" src="artwork/' . $row['artwork'] . '" alt="' . $row['album_name'] . '" width="90%" height="90%" />';
		echo '						</a>';
		echo '					</div>';
	}
}
else {
	echo 'Artist has no albums.';
}
echo '					</div>';
?>
			</div>
			<div>
					<h2>Tracks</h2>
<?php
echo '					<div class="whole">';
if($track_count != 0)
{
	while($row = mysql_fetch_array($tracks)) {
		echo '					<div class="half">';
		echo '						<a href="index.php?id=' . $row['track_id'] . '">'. $row['track_name'] .'</a>';
		echo '					</div>';
	}
}
else {
	echo 'Artist has no tracks.';
}
echo '					</div>';
?>
			</div>
<?php include('sites.php');?>
<?php include('events.php');?>
<?php include('twitter.php');?>
		</article>
		<aside class="outer">
		</aside>
	</section>
<?php
include('includes/footer.php'); 
?>