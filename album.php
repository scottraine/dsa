<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Albums";
$album_id = $_REQUEST['id'];
include('includes/header.php');

$artist = mysql_query("SELECT artist.artist_name as artist_name, artist.artist_id as id FROM artist INNER JOIN album_artist ON artist.artist_id = album_artist.artist_id WHERE album_artist.album_id = $album_id");
$artist_count = mysql_num_rows($artist);

$get_album = mysql_query("SELECT * FROM album WHERE album_id = '$album_id'");
$album_count = mysql_num_rows($get_album);
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<div class="third">
<?php

if($album_count != 0)
{
	$row = mysql_fetch_array($get_album);
	if($artist_count != 0) {
		$data = mysql_fetch_array($artist);
		echo '<h2>'. $row['album_name'] .' by <a href="artist.php?id=' . $data['id'] .'">' . $data['artist_name'] .'</a></h2>';
	}	
	echo '<img  class="artwork" src="artwork/' . $row['artwork'] . '" alt="' . $row['album_name'] . '" />';
}
else {
	echo 'Album does not exist.';
}

?>
			</div>
			<div class="two-thirds">
				<h2>Tracks</h2>
<?php
$tracks = mysql_query("SELECT * FROM track INNER JOIN track_album ON track.track_id = track_album.track_id WHERE album_id = '$album_id'");

$count = mysql_num_rows($tracks);

if($count != 0)
{
	while($row = mysql_fetch_array($tracks)) {
		echo '				<p>' . $row['track_no'] . ': <a href="index.php?id=' . $row['track_id'] . '">' . $row['track_name'] . '</a></p>';
	}
}
else {
	echo 'Album has no tracks';
}
?>			</div>
		</article>
		<aside class="outer">
<?php include('wiki.php');?>
		</aside>
	</section>
<?php
include('includes/footer.php'); 
?>