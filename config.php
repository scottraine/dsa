<?php
// -------------------------
// 0 - Side-wide settings
// -------------------------

// Set $local flag to TRUE if running on local machine, otherwise FALSE for live machine.
if (stristr($_SERVER['HTTP_HOST'], 'local') || (substr($_SERVER['HTTP_HOST'], 0, 7) == '192.168')) {
	$local = TRUE;
} else {
	$local = FALSE;
}

// Load functions file
include('functions.php');

// ---------------------------
// 1 - Database connections
// ---------------------------

// Multidimensional array to handle multiple environments,
// development used for local database connection and production,
// used for live database connection.
$db_vars = array(
    // Development
    array(
        'host'     => 'localhost',
        'username' => 'root',
        'password' => '',
        'database' => 'dsa'
    ),
    // Production - DO NOT MODIFY
    array(
        'host'     => 'mysql5.cems.uwe.ac.uk',
        'username' => 'fet11039315',
        'password' => 'w4rr10r',
        'database' => 'fet11039315'
    )
);

// Set environment
if ($local) {
    $db_vars = $db_vars[0];
}
else {
    $db_vars = $db_vars[1];
}

// Connect to MySQL
$connection = mysql_connect($db_vars['host'], $db_vars['username'], $db_vars['password']);
if (!$connection) {
    die("Database connection failed: " . mysql_error());
}

// Connect to DB
$select_db = mysql_select_db($db_vars['database'], $connection);
if (!$select_db) {
    die("Database selection failed:" . mysql_error());
}

/** Function: Get file through UWE proxy **/
function get_file($uri) {

/*********************************************************
 * @function: get_file
 * @author: Chris Wallace
 * @created: 30 November 2009
 * @updated: 20 January 2012
 * @source: http://www.cems.uwe.ac.uk/~pchatter/php/dsa/dsa_utility.phps
 *
 * This function will get any file through the UWE proxy. 
 *
 * It has been adapted so that if we
 * are running on our local testing server, we do not
 * need to use this function, as Ben's private server
 * does not have proxy requirements.
 *********************************************************/

    // Conditional: Do we need to use the proxy?
    if(stristr($_SERVER['HTTP_HOST'], 'cems.uwe.ac.uk')) { // Conditional @value: Yes

        // Create a context for the PHP file_get_contents function
        $context = stream_context_create(array('http'=> array('proxy'=>'proxysg.uwe.ac.uk:8080', 'header'=>'Cache-Control: no-cache'))); 

        // Get the contents of the requested URI
        $contents = file_get_contents($uri, false, $context); 

    } else { // Conditional @value: No

        // Get the contents of the requres URI without use of the proxy
        $contents = file_get_contents($uri, false);

    } // End Conditional

    // And return the contents of the file
    return $contents;

}


?>