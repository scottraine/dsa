<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Albums";
include('includes/header.php');
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
<?php
$query = mysql_query("SELECT * FROM album");
while($row = mysql_fetch_array($query)) {
	$album_id = $row['album_id'];
	$artist = mysql_query("SELECT artist_name, artist.artist_id FROM artist INNER JOIN album_artist ON artist.artist_id = album_artist.artist_id WHERE album_artist.album_id = $album_id");
	$artist_count = mysql_num_rows($artist);
	echo '			<div class="third">';
	if($artist_count != 0) {
		$data = mysql_fetch_array($artist);
		echo '				<a href="artist.php?id=' . $data['artist_id'] .'"><h2>' . $data['artist_name'] .'</h2></a>';
	}
	echo '				<a href="album.php?id=' . $row['album_id'] . '">';
	echo '					<p>' . $row['album_name'] . '</p>';
	echo '					<img class="artwork" src="artwork/' . $row['artwork'] . '" alt="' . $row['album_name'] . '" />';
	echo '				</a>';
	echo '			</div>';
}
?>
		</article>
		<aside class="outer">
		</aside>
	</section>

<?php include('includes/footer.php'); ?>