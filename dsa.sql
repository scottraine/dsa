-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2013 at 02:23 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dsa`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `artwork` varchar(255) DEFAULT NULL,
  `album_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6051313 ;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `artwork`, `album_name`) VALUES
(97419, 'Artwork%20-%20Ascendancy.jpg', 'Ascendancy'),
(80236, 'Artwork%20-%20As%20Daylight%20Dies.jpg', 'As Daylight Dies'),
(536299, 'Artwork%20-%20Diamond%20Eyes.jpg', 'Diamond Eyes'),
(6051312, 'Artwork%20-%20Koi%20No%20Yokan.jpg', 'Koi No Yokan'),
(95746, 'Artwork%20-%20The%20End%20of%20Heartache.jpg', 'The End of Heartache'),
(5979050, 'Artwork%20-%20Toxicity.jpg', 'Toxicity');

-- --------------------------------------------------------

--
-- Table structure for table `album_artist`
--

CREATE TABLE `album_artist` (
  `album_id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`artist_id`,`album_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_artist`
--

INSERT INTO `album_artist` (`album_id`, `artist_id`) VALUES
(5979050, 458),
(536299, 535),
(6051312, 535),
(97419, 1245),
(80236, 2124),
(95746, 2124);

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `artist_id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`artist_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2125 ;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`artist_id`, `artist_name`) VALUES
(535, 'Deftones'),
(2124, 'Killswitch Engage'),
(458, 'System of a Down'),
(1245, 'Trivium');

-- --------------------------------------------------------

--
-- Table structure for table `favourite_artist`
--

CREATE TABLE `favourite_artist` (
  `artist_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `favourite_date` datetime DEFAULT NULL,
  PRIMARY KEY (`artist_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourite_artist`
--

INSERT INTO `favourite_artist` (`artist_id`, `user_id`, `favourite_date`) VALUES
(2124, 3, '2013-02-14 19:38:39'),
(458, 3, '2013-02-14 16:19:44'),
(1245, 3, '2013-02-16 11:32:57'),
(535, 3, '2013-02-16 11:15:24'),
(535, 4, '2013-02-16 11:22:30'),
(2124, 4, '2013-02-16 11:46:30'),
(1245, 5, '2013-02-25 00:25:20'),
(2124, 5, '2013-02-24 20:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `favourite_track`
--

CREATE TABLE `favourite_track` (
  `track_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `favourite_date` datetime DEFAULT NULL,
  PRIMARY KEY (`track_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourite_track`
--

INSERT INTO `favourite_track` (`track_id`, `user_id`, `favourite_date`) VALUES
(3, 1, '2013-02-07 19:41:40'),
(56, 1, NULL),
(1, 3, '2013-02-14 19:47:41'),
(11, 3, '2013-02-14 19:49:17'),
(63, 3, '2013-02-14 19:51:05'),
(71, 3, '2013-02-14 19:56:13'),
(61, 3, '2013-02-16 10:59:08'),
(5, 4, '2013-02-16 13:12:07'),
(1, 5, '2013-02-24 23:04:48');

-- --------------------------------------------------------

--
-- Table structure for table `track`
--

CREATE TABLE `track` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `track_url` varchar(255) DEFAULT NULL,
  `track_no` int(11) DEFAULT NULL,
  `track_name` text,
  `track_added_date` date NOT NULL,
  PRIMARY KEY (`track_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `track`
--

INSERT INTO `track` (`track_id`, `track_url`, `track_no`, `track_name`, `track_added_date`) VALUES
(1, '01%20-%20Prison%20Song.mp3', 1, 'Prison Song', '2013-02-24'),
(2, '01%20-%20Swerve%20City.mp3', 1, 'Swerve City', '0000-00-00'),
(3, '01-deftones_-_diamond_eyes-ysp.mp3', 1, 'Diamond Eyes', '0000-00-00'),
(4, '01-killswitch_engage-a_bid_farewell-eos.mp3', 1, 'A Bid Farewell', '0000-00-00'),
(5, '01-The%20End%20of%20Everything.mp3', 1, 'The End of Everything', '0000-00-00'),
(6, '02%20-%20Needles.mp3', 2, 'Needles', '0000-00-00'),
(7, '02%20-%20Romantic%20Dreams.mp3', 2, 'Romantic Dreams', '0000-00-00'),
(8, '02-deftones_-_royal-ysp.mp3', 2, 'Royal', '0000-00-00'),
(9, '02-killswitch_engage-take_this_oath-eos.mp3', 2, 'Take This Oath', '0000-00-00'),
(10, '02-Rain.mp3', 2, 'Rain', '0000-00-00'),
(11, '03%20-%20Deer%20Dance.mp3', 3, 'Deer Dance', '0000-00-00'),
(12, '03%20-%20Leathers.mp3', 3, 'Leathers', '0000-00-00'),
(13, '03-deftones_-_cmnd-ctrl-ysp.mp3', 3, 'CMND/CTRL', '0000-00-00'),
(14, '03-killswitch_engage-when_darkness_falls-eos.mp3', 3, 'When Darkness Falls', '0000-00-00'),
(15, '03-Pull%20Harder%20on%20the%20Strings%20of%20Your%20Martyr.mp3', 3, 'Pull Harder on the Strings of Your Martyr', '0000-00-00'),
(16, '04%20-%20Jet%20Pilot.mp3', 4, 'Jet Pilot', '0000-00-00'),
(17, '04%20-%20Poltergeist.mp3', 4, 'Poltergeist', '0000-00-00'),
(18, '04-deftones_-_youve_seen_the_butcher-ysp.mp3', 4, 'You''ve Seen the Butcher', '0000-00-00'),
(19, '04-Drowned%20and%20Torn%20Asunder.mp3', 4, 'Drowned and Torn Asunder', '0000-00-00'),
(20, '04-killswitch_engage-rose_of_sharyn-eos.mp3', 4, 'Rose of Sharyn', '0000-00-00'),
(21, '05%20-%20Entombed.mp3', 5, 'Entombed', '0000-00-00'),
(22, '05%20-%20X.mp3', 5, 'X', '0000-00-00'),
(23, '05-Ascendancy.mp3', 5, 'Ascendancy', '0000-00-00'),
(24, '05-deftones_-_beauty_school-ysp.mp3', 5, 'Beauty School', '0000-00-00'),
(25, '05-killswitch_engage-inhale-eos.mp3', 5, 'Inhale', '0000-00-00'),
(26, '06%20-%20Chop%20Suey_.mp3', 6, 'Chop Suey', '0000-00-00'),
(27, '06%20-%20Graphic%20Nature.mp3', 6, 'Graphic Nature', '0000-00-00'),
(28, '06-A%20Gunshot%20to%20the%20Head%20of%20Trepidation.mp3', 6, 'A Gunshot to the Head of Trepidation', '0000-00-00'),
(29, '06-deftones_-_prince-ysp.mp3', 6, 'Prince', '0000-00-00'),
(30, '06-killswitch_engage-breathe_life-eos.mp3', 6, 'Breathe Life', '0000-00-00'),
(31, '07%20-%20Bounce.mp3', 7, 'Bounce', '0000-00-00'),
(32, '07%20-%20Tempest.mp3', 7, 'Tempest', '0000-00-00'),
(33, '07-deftones_-_rocket_skates-ysp.mp3', 7, 'Rocket Skates', '0000-00-00'),
(34, '07-killswitch_engage-the_end_of_heartache-eos.mp3', 7, 'The End of Heartache', '0000-00-00'),
(35, '07-Like%20Light%20to%20the%20Flies.mp3', 7, 'Like Light to the Flies', '0000-00-00'),
(36, '08%20-%20Forest.mp3', 8, 'Forest', '0000-00-00'),
(37, '08%20-%20Gauze.mp3', 8, 'Gauze', '0000-00-00'),
(38, '08-deftones_-_sextape-ysp.mp3', 8, 'Sextape', '0000-00-00'),
(39, '08-Dying%20in%20Your%20Arms.mp3', 8, 'Dying in Your Arms', '0000-00-00'),
(40, '08-killswitch_engage-declaration-eos.mp3\n', 8, 'Declaration', '0000-00-00'),
(41, '09%20-%20ATWA.mp3', 9, 'ATWA', '0000-00-00'),
(42, '09%20-%20Rosemary.mp3', 9, 'Rosemary', '0000-00-00'),
(43, '09-deftones_-_risk-ysp.mp3', 9, 'Risk', '0000-00-00'),
(44, '09-killswitch_engage-world_ablaze-eos.mp3', 9, 'World Ablaze', '0000-00-00'),
(45, '09-The%20Deceived.mp3', 9, 'The Deceived', '0000-00-00'),
(46, '10%20-%20Goon%20Squad.mp3', 10, 'Goon Squad', '0000-00-00'),
(47, '10%20-%20Science.mp3', 10, 'Science', '0000-00-00'),
(48, '10-deftones_-_976-evil-ysp.mp3', 10, 'Evil', '0000-00-00'),
(49, '10-killswitch_engage-and_embers_rise-eos.mp3', 10, 'Embers Rise', '0000-00-00'),
(50, '10-Suffocating%20Sight.mp3', 10, 'Suffocating Sight', '0000-00-00'),
(51, '11%20-%20Shimmy.mp3', 11, 'Shimmy', '0000-00-00'),
(52, '11%20-%20What%20Happened%20To%20You.mp3', 11, 'What Happened To You', '0000-00-00'),
(53, '11-deftones_-_this_place_is_death-ysp.mp3', 11, 'This Place is Death', '0000-00-00'),
(54, '11-Departure.mp3', 11, 'Departure', '0000-00-00'),
(55, '11-killswitch_engage-wasted_sacrifice-eos.mp3', 11, 'Wasted Sacrifice', '0000-00-00'),
(56, '12%20-%20Toxicity.mp3', 12, 'Toxicity', '0000-00-00'),
(57, '12-Declaration.mp3', 12, 'Declaration', '0000-00-00'),
(58, '12-killswitch_engage-hope_is___-eos.mp3', 12, 'Hope is...', '0000-00-00'),
(59, '13%20-%20Psycho.mp3', 13, 'Psycho', '0000-00-00'),
(60, '14%20-%20Aerials.mp3', 14, 'Aerials', '0000-00-00'),
(61, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2001%20Daylight%20Dies.mp3', 1, 'Daylight Dies', '0000-00-00'),
(62, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2002%20This%20is%20Absolution.mp3', 2, 'This Is Absolution', '0000-00-00'),
(63, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2003%20The%20Arms%20of%20Sorrow.mp3', 3, 'The Arms of Sorrow', '0000-00-00'),
(64, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2004%20Unbroken.mp3', 4, 'Unbroken', '0000-00-00'),
(65, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2005%20My%20Curse.mp3', 5, 'My Curse', '0000-00-00'),
(66, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2006%20For%20You.mp3', 6, 'For You', '0000-00-00'),
(67, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2007%20Still%20Beats%20Your%20Name.mp3', 7, 'Still Beats Your Name', '0000-00-00'),
(68, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2008%20Eye%20of%20the%20Storm.mp3', 8, 'Eye of the Storm', '0000-00-00'),
(69, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2009%20Break%20the%20Silence.mp3', 9, 'Break the Silence', '0000-00-00'),
(70, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2010%20Desperate%20Times.mp3', 10, 'Desperate Times', '0000-00-00'),
(71, 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2011%20Reject%20Yourself.mp3', 11, 'Reject Yourself', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `track_album`
--

CREATE TABLE `track_album` (
  `track_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  PRIMARY KEY (`album_id`,`track_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `track_album`
--

INSERT INTO `track_album` (`track_id`, `album_id`) VALUES
(61, 80236),
(62, 80236),
(63, 80236),
(64, 80236),
(65, 80236),
(66, 80236),
(67, 80236),
(68, 80236),
(69, 80236),
(70, 80236),
(71, 80236),
(4, 95746),
(9, 95746),
(14, 95746),
(20, 95746),
(25, 95746),
(30, 95746),
(34, 95746),
(40, 95746),
(44, 95746),
(49, 95746),
(55, 95746),
(58, 95746),
(5, 97419),
(10, 97419),
(15, 97419),
(19, 97419),
(23, 97419),
(28, 97419),
(35, 97419),
(39, 97419),
(45, 97419),
(50, 97419),
(54, 97419),
(57, 97419),
(3, 536299),
(8, 536299),
(13, 536299),
(18, 536299),
(24, 536299),
(29, 536299),
(33, 536299),
(38, 536299),
(43, 536299),
(48, 536299),
(53, 536299),
(1, 5979050),
(6, 5979050),
(11, 5979050),
(16, 5979050),
(22, 5979050),
(26, 5979050),
(31, 5979050),
(36, 5979050),
(41, 5979050),
(47, 5979050),
(51, 5979050),
(56, 5979050),
(59, 5979050),
(60, 5979050),
(2, 6051312),
(7, 6051312),
(12, 6051312),
(17, 6051312),
(21, 6051312),
(27, 6051312),
(32, 6051312),
(37, 6051312),
(42, 6051312),
(46, 6051312),
(52, 6051312);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `bio` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `hashed_password`, `salt`, `bio`) VALUES
(1, 'scott', 'scott.a.raine@gmail.com', 'dc010931cfad5a5e64db6e069a0719a65aeb9d0d', '1359734375', ''),
(3, 'example', 'example@example.com', '0da062c7a78138459c2e319bbf280446cec60e48', '1359734809', NULL),
(4, 'bruce', 'scott2.raine@live.uwe.ac.uk', '8ed3d3f92cc669bc757f84d18606a0f64fd31d3a', '1361013702', ''),
(5, 'Dave', 'justanotherdavemartin@gmail.com', 'bd7dbccc965441f2a4b4c8040f7ca7d0c0cbfb65', '1361045312', 'I  like the music');
