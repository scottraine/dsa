<?php
$query = "
(
SELECT artist_name AS name, username, favourite_artist.user_id, favourite_artist.artist_id, favourite_artist.favourite_date
FROM favourite_artist
INNER JOIN user ON favourite_artist.user_id = user.user_id
INNER JOIN artist ON favourite_artist.artist_id = artist.artist_id
)
UNION ALL
(
SELECT track_name, username, favourite_track.user_id, favourite_track.track_id, favourite_track.favourite_date
FROM favourite_track
INNER JOIN user ON favourite_track.user_id = user.user_id
INNER JOIN track ON favourite_track.track_id = track.track_id
)
ORDER BY favourite_date DESC 
LIMIT 20
";

$result = mysql_query($query);
?>			<div class="widget"><!-- Start of Widget -->
			<!-- RSS feed of favourites -->
				<h2>Last 20 Favourites:</h2>
<?php
while ($row = mysql_fetch_array($result)) {
	echo '			<div class="favourite">';
	echo '<p><a href="member.php?username=' . $row['username'] . '">' . $row['username'] . '</a> => <a href="' . isArtist($row['artist_id']) . '.php?id=' . $row['artist_id'] . '">'.$row['name'].'</a>';
	echo '			</div>';
}

?>
				<a class="button orange" href="rss.php">RSS</a>
			</div><!-- End of Widget -->
