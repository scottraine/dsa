<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Users";
include('includes/header.php');
$username = $_REQUEST['username'];

$gravatar = new Gravatar();
$email = $gravatar->get_email_for_user($username);

?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<div class="third">
				<h2><?php echo $username ?>'s profile</h2>
				<img src="<?php echo $gravatar->url($email); ?>" class="avatar"/>
<?php
$query = mysql_query("SELECT * FROM user WHERE username = '$username'");
$count = mysql_num_rows($query);

if($count != 0)
{
	$row = mysql_fetch_array($query);
	$user_id = $row['user_id'];
	echo '				<dl">';
	echo '					<dt>Username: </dt><dd>' . $row['username'] . '</dd>';
	echo '					<dt>E-mail: </dt><dd>' . $row['email'] . '</dd>';
	echo '					<dt>Bio: </dt><dd>' . $row['bio'] . '</dd>';
	echo '				</dl>';
}
else {
	echo $username . ' does not exist.';
}
if($username == $_SESSION['username']) {
	echo '<a href="edit.php" class="button blue">Edit profile</a>';
}
?>
			</div>
			<div class="third">
				<h2>Favourite Artists</h2>
				<table>
					<tbody>
<?php
$artists = mysql_query("SELECT * FROM artist INNER JOIN favourite_artist ON artist.artist_id = favourite_artist.artist_id WHERE user_id = '$user_id' ORDER BY favourite_date DESC");

$count = mysql_num_rows($artists);

if($count != 0)
{
	while($row = mysql_fetch_array($artists)) {
		echo '						<tr>';
		echo '							<td class="half"><a href="artist.php?id=' . $row['artist_id'] . '">' . $row['artist_name'] . '</a></td>';
		echo '							<td class="half">' . $row['favourite_date'] . '</td>';
		echo '						</tr>';
	}
}
else {
	echo '<p>User is not following any artists.</p>';
}
?>
				  </tbody>
				</table>
			</div>
			<div class="third">
				<h2>Favourite Tracks</h2>
				<table>
				 	<tbody>
<?php
$tracks = mysql_query("SELECT * FROM track INNER JOIN favourite_track ON track.track_id = favourite_track.track_id WHERE user_id = '$user_id' ORDER BY favourite_date DESC");

$count = mysql_num_rows($tracks);

if($count != 0)
{
	while($row = mysql_fetch_array($tracks)) {
		echo '						<tr>';
		echo '							<td class="half"><a href="index.php?id=' . $row['track_id'] . '">' . $row['track_name'] . '</a></td>';
		echo '							<td class="half">' . $row['favourite_date'] . '</td>';
		echo '						</tr>';
	}
}
else {
	echo '<p>User is not following any tracks.</p>';
}
?>
					</tbody>
				</table>
			</div>
		</article>
		<aside>
		</aside>
	<section>
<?php
include('includes/footer.php'); 
?>