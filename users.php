<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Users";
include('includes/header.php');
$query = "
(
SELECT artist_name AS name, username, favourite_artist.user_id, favourite_artist.artist_id, favourite_artist.favourite_date
FROM favourite_artist
INNER JOIN user ON favourite_artist.user_id = user.user_id
INNER JOIN artist ON favourite_artist.artist_id = artist.artist_id
)
UNION ALL
(
SELECT track_name, username, favourite_track.user_id, favourite_track.track_id, favourite_track.favourite_date
FROM favourite_track
INNER JOIN user ON favourite_track.user_id = user.user_id
INNER JOIN track ON favourite_track.track_id = track.track_id
)
ORDER BY favourite_date DESC 
LIMIT 5
";

$result = mysql_query($query);
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="third">
			<table>
				<thead>
					<th class="fifth"></th>
				    <th class="fifth">Users:</th>
					</thead>
  				<tbody>
<?php
$query = mysql_query("SELECT * FROM user");
while($row = mysql_fetch_array($query)) {
	$username = $row['username'];
	$gravatar = new Gravatar();
	$email = $gravatar->get_email_for_user($username);
	echo '					<tr>';
	echo '						<td class="fifth"><a href="member.php?username=' . $username . '">' . $username . '</a></td>';
	echo '						<td class="fifth"><img src=" ' . $gravatar->url($email, 48) . '" /></td>';
	echo '					</tr>';
}
?>
				</tbody>
			</table>
		</article>
		<aside class="half">
<?php include('news.php');?>
		</aside>
	</section>

<?php include('includes/footer.php'); ?>