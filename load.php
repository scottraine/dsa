<?php 
$mysqli = new mysqli();
$mysqli->connect('localhost', 'root', '', 'dsa2'); 

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}


//$query ="DROP TABLE favourite_artist;";
//$query .="DROP TABLE favourite_track;";
//$query .="DROP TABLE track;";
//$query .="DROP TABLE album;";
//$query .="DROP TABLE album_artist;";
//$query .="DROP TABLE artist;";
//$query .="DROP TABLE user;";

$query ="CREATE TABLE album (album_id INTEGER AUTO_INCREMENT, artwork VARCHAR(255), album_name VARCHAR(255), album_artist_id INTEGER, PRIMARY KEY (album_id));"; 

$query .="CREATE TABLE artist (artist_id INTEGER AUTO_INCREMENT,artist_name VARCHAR(255),PRIMARY KEY (artist_id));"; 
$query .="CREATE TABLE album_artist (album_artist_id INTEGER AUTO_INCREMENT,artist_id INTEGER,album_artist VARCHAR(255),PRIMARY KEY (album_artist_id));"; 
$query .="CREATE TABLE track (track_id INTEGER AUTO_INCREMENT,album_id INTEGER,track_name VARCHAR(255),track_no INTEGER,track_date DATETIME,PRIMARY KEY (track_id));";
$query .="CREATE TABLE user (user_id INTEGER AUTO_INCREMENT,username VARCHAR(255),email VARCHAR(255),hashed_password VARCHAR(255),salt VARCHAR(255),location VARCHAR(255),bio VARCHAR(255),PRIMARY KEY (user_id));"; 

$query .="CREATE TABLE favourite_track (track_id INTEGER,user_id INTEGER,favourite_date DATETIME,PRIMARY KEY (track_id,user_id));"; 
$query .="CREATE TABLE favourite_artist (artist_id INTEGER,user_id INTEGER,favourite_date DATETIME,PRIMARY KEY (artist_id,user_id));"; 

//load artists

$query .="INSERT INTO artist (artist_name) VALUES ('Deftones');"; 
$query .="INSERT INTO artist (artist_name) VALUES ('Killswitch Engage');"; 
$query .="INSERT INTO artist (artist_name) VALUES ('System of a Down');"; 
$query .="INSERT INTO artist (artist_name) VALUES ('Trivium');";

//load album_artist

$query .="INSERT INTO album_artist (artist_id, album_artist) VALUES ((SELECT artist_id FROM artist WHERE artist.artist_name = 'Deftones'),'Deftones');"; 
$query .="INSERT INTO album_artist (artist_id, album_artist) VALUES ((SELECT artist_id FROM artist WHERE artist.artist_name = 'Killswitch Engage'),'Killswitch Engage');"; 
$query .="INSERT INTO album_artist (artist_id, album_artist) VALUES ((SELECT artist_id FROM artist WHERE artist.artist_name = 'System of a Down'),'System of a Down');"; 
$query .="INSERT INTO album_artist (artist_id, album_artist) VALUES ((SELECT artist_id FROM artist WHERE artist.artist_name = 'Trivium'),'Trivium');";

//load albums

$query .="INSERT INTO album (artwork, album_name, album_artist_id) VALUES ('Artwork%20-%20Ascendancy.jpg', 'Ascendancy', (SELECT album_artist_id FROM album_artist WHERE album_artist.album_artist = 'Trivium'));"; 
$query .="INSERT INTO album (artwork, album_name, album_artist_id) VALUES ('Artwork%20-%20As%20Daylight%20Dies.jpg', 'As Daylight Dies', (SELECT album_artist_id FROM album_artist WHERE album_artist.album_artist = 'Killswitch Engage'));"; 
$query .="INSERT INTO album (artwork, album_name, album_artist_id) VALUES ('Artwork%20-%20Diamond%20Eyes.jpg', 'Diamond Eyes', (SELECT album_artist_id FROM album_artist WHERE album_artist.album_artist = 'Deftones'));"; 
$query .="INSERT INTO album (artwork, album_name, album_artist_id) VALUES ('Artwork%20-%20Koi%20No%20Yokan.jpg', 'Koi No Yokan', (SELECT album_artist_id FROM album_artist WHERE album_artist.album_artist = 'Deftones'));"; 
$query .="INSERT INTO album (artwork, album_name, album_artist_id) VALUES ('Artwork%20-%20The%20End%20of%20Heartache.jpg', 'The End of Heartache', (SELECT album_artist_id FROM album_artist WHERE album_artist.album_artist = 'Killswitch Engage'));"; 
$query .="INSERT INTO album (artwork, album_name, album_artist_id) VALUES ('Artwork%20-%20Toxicity.jpg', 'Toxicity', (SELECT album_artist_id FROM album_artist WHERE album_artist.album_artist = 'System of a Down'));";

//load ALL the tracks!

$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '01%20-%20Prison%20Song.mp3', 1, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '01%20-%20Swerve%20City.mp3', 1, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '01-deftones_-_diamond_eyes-ysp.mp3', 1, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '01-killswitch_engage-a_bid_farewell-eos.mp3', 1, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '01-The%20End%20of%20Everything.mp3', 1, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '02%20-%20Needles.mp3', 2, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '02%20-%20Romantic%20Dreams.mp3', 2, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '02-deftones_-_royal-ysp.mp3', 2, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '02-killswitch_engage-take_this_oath-eos.mp3', 2, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '02-Rain.mp3', 2, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '03%20-%20Deer%20Dance.mp3', 3, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '03%20-%20Leathers.mp3', 3, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '03-deftones_-_cmnd-ctrl-ysp.mp3', 3, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '03-killswitch_engage-when_darkness_falls-eos.mp3', 3, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '03-Pull%20Harder%20on%20the%20Strings%20of%20Your%20Martyr.mp3', 3, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '04%20-%20Jet%20Pilot.mp3', 4, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '04%20-%20Poltergeist.mp3', 4, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '04-deftones_-_youve_seen_the_butcher-ysp.mp3', 4, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '04-Drowned%20and%20Torn%20Asunder.mp3', 4, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '04-killswitch_engage-rose_of_sharyn-eos.mp3', 4, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '05%20-%20Entombed.mp3', 5, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '05%20-%20X.mp3', 5, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '05-Ascendancy.mp3', 5, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '05-deftones_-_beauty_school-ysp.mp3', 5, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '05-killswitch_engage-inhale-eos.mp3', 5, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '06%20-%20Chop%20Suey_.mp3', 6, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '06%20-%20Graphic%20Nature.mp3', 6, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '06-A%20Gunshot%20to%20the%20Head%20of%20Trepidation.mp3', 6, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '06-deftones_-_prince-ysp.mp3', 6, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '06-killswitch_engage-breathe_life-eos.mp3', 6, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '07%20-%20Bounce.mp3', 7, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '07%20-%20Tempest.mp3', 7, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '07-deftones_-_rocket_skates-ysp.mp3', 7, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '07-killswitch_engage-the_end_of_heartache-eos.mp3', 7, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '07-Like%20Light%20to%20the%20Flies.mp3', 7, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '08%20-%20Forest.mp3', 8, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '08%20-%20Gauze.mp3', 8, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '08-deftones_-_sextape-ysp.mp3', 8, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '08-Dying%20in%20Your%20Arms.mp3', 8, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '08-killswitch_engage-declaration-eos.mp3
', 8, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '09%20-%20ATWA.mp3', 9, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '09%20-%20Rosemary.mp3', 9, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '09-deftones_-_risk-ysp.mp3', 9, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '09-killswitch_engage-world_ablaze-eos.mp3', 9, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '09-The%20Deceived.mp3', 9, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '10%20-%20Goon%20Squad.mp3', 10, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '10%20-%20Science.mp3', 10, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '10-deftones_-_976-evil-ysp.mp3', 10, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '10-killswitch_engage-and_embers_rise-eos.mp3', 10, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '10-Suffocating%20Sight.mp3', 10, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '11%20-%20Shimmy.mp3', 11, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Koi No Yokan'), '11%20-%20What%20Happened%20To%20You.mp3', 11, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Diamond Eyes'), '11-deftones_-_this_place_is_death-ysp.mp3', 11, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '11-Departure.mp3', 11, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '11-killswitch_engage-wasted_sacrifice-eos.mp3', 11, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '12%20-%20Toxicity.mp3', 12, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Ascendancy'), '12-Declaration.mp3', 12, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'The End of Heartache'), '12-killswitch_engage-hope_is___-eos.mp3', 12, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '13%20-%20Psycho.mp3', 13, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'Toxicity'), '14%20-%20Aerials.mp3', 14, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2001%20Daylight%20Dies.mp3', 1, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2002%20This%20is%20Absolution.mp3', 2, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2003%20The%20Arms%20of%20Sorrow.mp3', 3, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2004%20Unbroken.mp3', 4, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2005%20My%20Curse.mp3', 5, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2006%20For%20You.mp3', 6, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2007%20Still%20Beats%20Your%20Name.mp3', 7, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2008%20Eye%20of%20the%20Storm.mp3', 8, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2009%20Break%20the%20Silence.mp3', 9, '2013-01-17 10:38:00');"; 
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2010%20Desperate%20Times.mp3', 10, '2013-01-17 10:38:00');";
$query .="INSERT INTO track (album_id, track_name, track_no, track_date) VALUES ((SELECT album_id FROM album WHERE album.album_name = 'As Daylight Dies'), 'Killswitch%20Engage%20-%20As%20Daylight%20Dies%2011%20Reject%20Yourself.mp3', 11, '2013-01-17 10:38:00');";

//foreign key 

$query .="ALTER TABLE album ADD FOREIGN KEY album_artist_id (album_artist_id) REFERENCES album_artist (album_artist_id);"; 
$query .="ALTER TABLE album_artist ADD FOREIGN KEY artist_id (artist_id) REFERENCES artist (artist_id);"; 
$query .="ALTER TABLE track ADD FOREIGN KEY album_id (album_id) REFERENCES album (album_id);"; 
$query .="ALTER TABLE user ADD FOREIGN KEY track_id (track_id) REFERENCES track (track_id);"; 
$query .="ALTER TABLE favourite_track ADD FOREIGN KEY track_id (track_id) REFERENCES track (track_id);"; 
$query .="ALTER TABLE favourite_track ADD FOREIGN KEY user_id (user_id) REFERENCES user (user_id);"; 
$query .="ALTER TABLE favourite_artist ADD FOREIGN KEY artist_id (artist_id) REFERENCES artist (artist_id);"; 
$query .="ALTER TABLE favourite_artist ADD FOREIGN KEY user_id (user_id) REFERENCES user (user_id)";


//execute multi query
if ($mysqli->multi_query($query)) {
    do {
//store first result set
        if ($result = $mysqli->store_result()) {
            while ($row = $result->fetch_row()) {
                printf("%s\n", $row[0]);
            }
            $result->free();
        }
//print divider
        if ($mysqli->more_results()) {
            printf("-----------------\n");
        }
    } while ($mysqli->next_result());
}

// close connection
$mysqli->close();
?>