<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Login";
include('config.php');
if(isset($_POST['submit'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];

	$query = mysql_query("SELECT * FROM user WHERE username='$username'");
	if (!$query) {
		exit("$username does not exist.");
	}
	$row = mysql_fetch_array($query);
	$hashed_password = $row['hashed_password'];
	$salt = $row['salt'];

	$hash_password = sha1($password . $salt);
	if ($hashed_password != $hash_password) {
		exit("The password you entered is incorrect");
	}
	else {
		$_SESSION['username'] = $username;
		$_SESSION['password'] = $hash_password;
		header("Location: member.php?username=$username");
	}
}
include('includes/header.php'); 
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<div class="third">
				<form method="post">
					<label class="whole" for="id_username">Username:</label>
					<input class="whole" type="text" name="username" />
					<label class="whole" for="id_password">Password:</label>
					<input class="whole" type="password" name="password" />
					<input class="button blue" type="submit" name="submit" value="Login" id="submit"/>
				</form>
			</div>
			<div class="third">
				<h2>Not registered?</h2>
				<a class="button orange" href="register.php">Register</a>
			</div>
		</article>
		<aside>
		</aside>
	</section>
<?php include('includes/footer.php'); ?>