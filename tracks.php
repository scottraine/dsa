<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

include('includes/header.php');
include('mainnav.php');
?>
<h2>Tracks</h2>
<hr>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Track</th>
    </tr>
  </thead>
  <tbody>
	<?php
	$query = mysql_query("SELECT * FROM track");
	while($row = mysql_fetch_array($query)) {
		echo '<tr>';
		echo '<td><a href="track.php?id=' . $row['track_id'] . '">' . $row['track_name'] . '</a></td>';
		echo '</tr>';
	}
	?>
  </tbody>
</table>
<?php include('includes/footer.php'); ?>