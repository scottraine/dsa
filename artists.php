<?php
session_start();

// Server configuration issue messed up session data, fixed provided by:
// http://www.php.net/manual/en/reserved.variables.session.php#85448
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}

$page_title = "Artists";
include('includes/header.php');
?>
	<section>
		<header>
			<h1><?php echo $page_title ?></h1>
		</header>
		<?php include('mainnav.php');?>
		<article id="main" class="inner">
			<table>
				<thead>
		    		<th class="fifth">Artist</th>
					<th class="fifth">Favourited by</th>
					<th class="fifth">Number of Albums</th>
				</thead>
				<tbody>
<?php
$query = mysql_query("SELECT * FROM artist");
while($row = mysql_fetch_array($query)) {
	$artist_id = $row['artist_id'];
	$users = mysql_query("SELECT username FROM user INNER JOIN favourite_artist ON favourite_artist.user_id = user.user_id WHERE favourite_artist.artist_id = $artist_id ORDER BY favourite_artist.favourite_date DESC LIMIT 5");
	$users_count = mysql_num_rows($users);
	$count_albums = mysql_query("SELECT * FROM album INNER JOIN album_artist ON album_artist.album_id = album.album_id WHERE album_artist.artist_id = $artist_id");
	$num_albums = mysql_num_rows($count_albums);
	echo '					<tr>';
	echo '						<td class="fifth"><a href="artist.php?id=' . $artist_id . '">' . $row['artist_name'] . '</a></td>';
	echo '						<td class="fifth">';
	if ($users_count != 0) {
		while($user = mysql_fetch_array($users)) {
			$username = $user['username'];
			$gravatar = new Gravatar();
			$email = $gravatar->get_email_for_user($username);
			echo '<a href="member.php?username=' . $username . '"><img src=" ' . $gravatar->url($email, 24) . '"/></a>';
		}
	}
	else {
		echo 'Nobody';
	}
	echo '						</td>';
	echo '						<td class="fifth">' . $num_albums .'</td>';
	echo '					</tr>';
}
?>
			  </tbody>
			</table>
		<aside class="outer">
			<!-- Lloyd's Widget? -->
		</aside>
	</section>
<?php include('includes/footer.php'); ?>