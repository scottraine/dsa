<?php
?>
			<div class="widget"><!--  Start of Widget -->
				<h1>Events</h1>
				<table>
					<thead>
						<th class="fifth">Artist</th>
						<th class="fifth">Venue</th>
						<th class="fifth">Location</th>
						<th class="fifth">Date &amp; Time</th>
						<th class="fifth">Ticket Status</th>
					</thead>
					<tbody>
<?php
	// Events widget - By Scott Raine 10008285
	
	// URLify the artist name.
	$artist_url = str_replace(' ', '%20', $artist_name);
	
	// Construct API url
	$api_url = 'http://api.bandsintown.com/artists/' . $artist_url . '/events.json?api_version=2.0&app_id=dsa-music-app&date=upcoming';
	
	// Grab JSON file, using the get_file function to proxy through the UWE firewall.
	$json = get_file($api_url, 0, null, null);
	// Parse the JSON.
	$json_output = json_decode($json, true);
	
	// Loop through parsed JSON, and render the selected nodes.
	foreach ($json_output as $event) {
		echo '					<tr>';
	    echo '						<td class="fifth">' . $event['artists'][0]['name'] . '</td>';
		echo '						<td class="fifth">' . $event['venue']['name'] . '</td>';
		echo '						<td class="fifth">' . $event['formatted_location'] .'</td>';
		echo '						<td class="fifth">' . $event['formatted_datetime'] . '</td>';
		echo '						<td class="fifth">' . $event['ticket_status'] . '</td>';
		echo '					</tr>';
	}
	// End Events widget
?>
					</tbody>
				</table>
			</div><!--  End of Widget -->
